# App use (as examples) during the Bootcamp to show case different deployment and more

java-gradle-app
Git repo: https://gitlab.com/twn-devops-bootcamp/latest/06-nexus/java-app.git java-gradle-app

java-maven-app
Git repo: git@gitlab.com:twn-devops-bootcamp/latest/04-build-tools/java-maven-app.git

node-app
Git repo: git@gitlab.com:twn-devops-bootcamp/latest/04-build-tools/node-app.git

react-nodejs-example
Git repo: https://github.com/techworld-with-nana/react-nodejs-example.git
