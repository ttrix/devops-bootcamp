# Git Cheat Sheet

## Initial configuration

'''
git config --global user.name "git_username"
git config --global user.email "git_at_email.com"
git config --global core.editor "vim"
'''
