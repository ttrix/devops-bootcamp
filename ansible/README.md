
## extra variables
```
-e "var1=value1 vars2=value2"
```
This is called "Survey" in AAP

## sudo privilege

add this when defining the task
```
become: yes
```
This is called "Privilege Escalation" in AAP
